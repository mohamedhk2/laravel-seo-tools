# Laravel Seo Tools

[![](http://poser.pugx.org/mohamedhk2/laravel-seo-tools/v)](https://packagist.org/packages/mohamedhk2/laravel-seo-tools)
[![Total Downloads](http://poser.pugx.org/mohamedhk2/laravel-seo-tools/downloads)](https://packagist.org/packages/mohamedhk2/laravel-seo-tools)
[![Latest Unstable Version](http://poser.pugx.org/mohamedhk2/laravel-seo-tools/v/unstable)](https://packagist.org/packages/mohamedhk2/laravel-seo-tools)
[![License](http://poser.pugx.org/mohamedhk2/laravel-seo-tools/license)](https://packagist.org/packages/mohamedhk2/laravel-seo-tools)

## Packages

|                                                                       |                                                                                                       |
|-----------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------|
| [artesaos/seotools](https://packagist.org/packages/artesaos/seotools) | ![](http://poser.pugx.org/artesaos/seotools/v) ![](http://poser.pugx.org/artesaos/seotools/downloads) |
| [spatie/schema-org](https://packagist.org/packages/spatie/schema-org) | ![](http://poser.pugx.org/spatie/schema-org/v) ![](http://poser.pugx.org/spatie/schema-org/downloads) |

## Install

The recommended way to install this is through composer:

```bash
composer require "mohamedhk2/laravel-seo-tools"
```

## Usage

... coming soon ...

## License

The Laravel Seo Tools is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
