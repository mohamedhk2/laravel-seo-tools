<?php

namespace Mohamedhk2\LaravelSeoTools\Traits;

use Mohamedhk2\LaravelSeoTools\Interfaces\HealthAndBeautyBusinessInterface;
use Spatie\SchemaOrg\BaseType;
use Spatie\SchemaOrg\Contracts\ImageObjectContract;
use Spatie\SchemaOrg\Contracts\OpeningHoursSpecificationContract;
use Spatie\SchemaOrg\HealthAndBeautyBusiness;
use Spatie\SchemaOrg\Schema;

/**
 * @mixin HealthAndBeautyBusinessInterface
 */
trait HealthAndBeautyBusinessTrait
{
	use BaseTrait;

	/**
	 * @return HealthAndBeautyBusiness
	 * @throws \Exception
	 */
	public function seoShema(): HealthAndBeautyBusiness
	{
		$shema = Schema::healthAndBeautyBusiness()
			->name($this->getSeo_Name())
			->image($this->getSeo_Image() instanceof BaseType ? $this->getSeo_Image()->toArray() : $this->getSeo_Image())
			->description($this->getSeo_Description())
			->url($this->getSeo_Url())
			->telephone($this->getSeo_Telephone())
			->address($this->getSeo_Address() instanceof BaseType ? $this->getSeo_Address()->toArray() : $this->getSeo_Address());
		if ($this->getSeo_Geo()) $shema->geo($this->getSeo_Geo());
		if (count($this->getSeo_OpeningHoursSpecifications())) {
			foreach ($this->getSeo_OpeningHoursSpecifications() as $openingHoursSpecification)
				if (!$openingHoursSpecification instanceof OpeningHoursSpecificationContract)
					throw new \Exception('OpeningHoursSpecificationContract is required');
			$shema->openingHoursSpecification($this->getSeo_OpeningHoursSpecifications());
		}
		return $shema;
	}
}
