<?php

namespace Mohamedhk2\LaravelSeoTools\Traits;

use Mohamedhk2\LaravelSeoTools\Interfaces\ServiceInterface;
use Spatie\SchemaOrg\BaseType;
use Spatie\SchemaOrg\Contracts\ImageObjectContract;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\Service;

/**
 * @mixin ServiceInterface
 */
trait ServiceTrait
{
	use BaseTrait;

	/**
	 * @return Service
	 * @throws \Exception
	 */
	public function seoShema(): Service
	{
		$shema = Schema::Service()
			->name($this->getSeo_Name())
			->image($this->getSeo_Image() instanceof ImageObjectContract ? $this->getSeo_Image()->toArray() : $this->getSeo_Image())
			->description($this->getSeo_Description())
			->url($this->getSeo_Url());
		if ($this->getSeo_Category() !== null) {
			if ($this->getSeo_Category() instanceof BaseType) {
				$shema->category($this->getSeo_Category()->toArray());
			} else {
				$shema->category($this->getSeo_Category());
			}
		}
		if ($this->getSeo_AreaServed() !== null) {
			if ($this->getSeo_AreaServed() instanceof BaseType) {
				$shema->areaServed($this->getSeo_AreaServed()->toArray());
			} else {
				$shema->areaServed($this->getSeo_AreaServed());
			}
		}
		if ($this->getSeo_Offers() !== null) {
			if ($this->getSeo_Offers() instanceof BaseType) {
				$shema->offers($this->getSeo_Offers()->toArray());
			} else {
				$shema->offers($this->getSeo_Offers());
			}
		}
		if ($this->getSeo_AggregateRating() !== null) {
			if ($this->getSeo_AggregateRating() instanceof BaseType) {
				$shema->aggregateRating($this->getSeo_AggregateRating()->toArray());
			} else {
				$shema->aggregateRating($this->getSeo_AggregateRating());
			}
		}
		if ($this->getSeo_Review() !== null) {
			if ($this->getSeo_Review() instanceof BaseType) {
				$shema->review($this->getSeo_Review()->toArray());
			} else {
				$shema->review($this->getSeo_Review());
			}
		}
		return $shema;
	}
}
