<?php

namespace Mohamedhk2\LaravelSeoTools\Traits;

use Spatie\SchemaOrg\HealthAndBeautyBusiness;
use Spatie\SchemaOrg\Type;

trait BaseTrait
{
	/**
	 * @return array
	 * @throws \Exception
	 */
	public function toSeoArray(): array
	{
		return $this->seoShema()->toArray();
	}

	/**
	 * @return HealthAndBeautyBusiness
	 * @throws \Exception
	 */
	abstract public function seoShema(): Type;

	/**
	 * @return string
	 * @throws \Exception
	 */
	public function toSeoJson(): string
	{
		return $this->seoShema()->toScript();
	}

}
