<?php

namespace Mohamedhk2\LaravelSeoTools\Interfaces;

interface SeoOutputInterface
{
	public function toSeoArray(): array;

	public function toSeoJson(): string;
}
