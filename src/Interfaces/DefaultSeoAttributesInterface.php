<?php

namespace Mohamedhk2\LaravelSeoTools\Interfaces;

use Spatie\SchemaOrg\ImageObject;

interface DefaultSeoAttributesInterface
{
	/**
	 * The name of the item.
	 * @return string
	 */
	function getSeo_Name(): string;

	/**
	 * An image of the item. This can be a URL or a fully described ImageObject.
	 * @return string|ImageObject
	 */
	function getSeo_Image(): string|ImageObject;

	/**
	 * A description of the item.
	 * @return string
	 */
	function getSeo_Description(): string;

	/**
	 * URL of the item.
	 * @return string
	 */
	function getSeo_Url(): string;
}
