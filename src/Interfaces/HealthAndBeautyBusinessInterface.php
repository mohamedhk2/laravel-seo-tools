<?php

namespace Mohamedhk2\LaravelSeoTools\Interfaces;

use Spatie\SchemaOrg\GeoCoordinates;
use Spatie\SchemaOrg\GeoShape;
use Spatie\SchemaOrg\OpeningHoursSpecification;
use Spatie\SchemaOrg\PostalAddress;

/**
 * @see https://schema.org/HealthAndBeautyBusiness
 */
interface HealthAndBeautyBusinessInterface extends DefaultSeoAttributesInterface, SeoOutputInterface
{
	/**
	 * The telephone number.
	 * @return string
	 */
	function getSeo_Telephone(): string;

	/**
	 * Physical address of the item.
	 * @return PostalAddress|string
	 */
	function getSeo_Address(): PostalAddress|string;

	/**
	 * The geo coordinates of the place.
	 * @return GeoCoordinates|GeoShape|null
	 */
	function getSeo_Geo(): GeoCoordinates|GeoShape|null;

	/**
	 * The opening hours of a certain place.
	 * @return OpeningHoursSpecification[]
	 */
	function getSeo_OpeningHoursSpecifications(): array;
}
