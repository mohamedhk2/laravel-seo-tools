<?php

namespace Mohamedhk2\LaravelSeoTools\Interfaces;

use Spatie\SchemaOrg\AdministrativeArea;
use Spatie\SchemaOrg\AggregateRating;
use Spatie\SchemaOrg\CategoryCode;
use Spatie\SchemaOrg\Demand;
use Spatie\SchemaOrg\GeoShape;
use Spatie\SchemaOrg\Offer;
use Spatie\SchemaOrg\PhysicalActivityCategory;
use Spatie\SchemaOrg\Place;
use Spatie\SchemaOrg\Review;

/**
 * @see https://schema.org/Service
 */
interface ServiceInterface extends DefaultSeoAttributesInterface, SeoOutputInterface
{
	/**
	 * A category for the item. Greater signs or slashes can be used to informally indicate a category hierarchy.
	 * @return CategoryCode|PhysicalActivityCategory|string|null
	 */
	public function getSeo_Category(): CategoryCode|PhysicalActivityCategory|string|null;

	/**
	 * The geographic area where a service or offered item is provided.
	 * @return AdministrativeArea|GeoShape|Place|string|array|null
	 */
	public function getSeo_AreaServed(): AdministrativeArea|GeoShape|Place|string|array|null;

	/**
	 * An offer to provide this item—for example, an offer to sell a product, rent the DVD of a movie,
	 * perform a service, or give away tickets to an event. Use businessFunction to indicate the kind
	 * of transaction offered, i.e. sell, lease, etc. This property can also be used to describe a Demand.
	 * While this property is listed as expected on a number of common types, it can be used in others.
	 * In that case, using a second type, such as Product or a subtype of Product, can clarify the nature of the offer.
	 * @return Demand|Offer|array|null
	 */
	public function getSeo_Offers(): Demand|Offer|array|null;

	/**
	 * The overall rating, based on a collection of reviews or ratings, of the item.
	 * @return AggregateRating|null
	 */
	public function getSeo_AggregateRating(): AggregateRating|null;

	/**
	 * A review of the item. Supersedes reviews.
	 * @return Review|null
	 */
	public function getSeo_Review(): Review|null;
}
